package hybrid.peddle.ui.base

import android.animation.Animator
import androidx.transition.Slide
import android.view.ViewGroup

class SlideInOnly(slideEdge: Int) : Slide(slideEdge) {
    override fun onDisappear(sceneRoot: ViewGroup?, startValues: androidx.transition.TransitionValues?, startVisibility: Int, endValues: androidx.transition.TransitionValues?, endVisibility: Int): Animator? {
        return null
    }
}