package hybrid.peddle.repository.models

import java.math.BigDecimal

data class ProductItem(var itemId: String, var name: String, var category: String, var price: BigDecimal)

