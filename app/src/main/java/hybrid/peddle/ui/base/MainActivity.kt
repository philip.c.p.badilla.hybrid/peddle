package hybrid.peddle.ui.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.transition.TransitionManager
import com.google.android.material.navigation.NavigationView
import hybrid.peddle.R
import hybrid.peddle.ui.*
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : InjectorActivity() {
    @Inject
    lateinit var rxState: RxState

    @Inject
    lateinit var dispatcher: Dispatcher

    @Inject
    lateinit var myScreenCreator: MyScreenCreator

    private lateinit var drawerLayout: DrawerLayout

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityComponent.inject(this)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.visibility = View.GONE
        setSupportActionBar(toolbar)

        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_white)
        }
        drawerLayout = findViewById(R.id.drawerLayout)

        val navigationView: NavigationView = findViewById(R.id.navView)

        navigationView.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true // Persist menu item highlight
            drawerLayout.closeDrawers()

            when(menuItem.itemId){
                R.id.navCheckout -> { dispatcher.dispatch(Screen.ItemPicker) }
                R.id.navProducts -> { dispatcher.dispatch(Screen.ProductCatalog) }
                R.id.navCustomers -> { }
                R.id.navTransactions -> { }
                R.id.navAnalytics -> { }
                R.id.navUsers -> { dispatcher.dispatch(Screen.UserCatalog) }
                R.id.navSettings -> { }
            }
            // Add code here to update the UI based on the item selected
            // For example, swap UI fragments here

            true
        }

        val switchUserPanel: ConstraintLayout = findViewById(R.id.switchUserPanel)
        switchUserPanel.setOnClickListener {
            dispatcher.clearBackStack()
            dispatcher.dispatch(State.SignOut)
            dispatcher.dispatch(Screen.PinEntry)
        }

        val footerUserFullName: TextView =findViewById(R.id.footerUserFullName)

        rxState.backStackEmpty().subscribe { setResult(Activity.RESULT_OK, Intent()); finish() }

        rxState.ofType(State.SessionStart::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    footerUserFullName.text = it.user.name
                    toolbar.visibility = View.VISIBLE
                }
        
        rxState.ofType(State.SignOut::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    footerUserFullName.text = ""
                    drawerLayout.closeDrawers()
                    toolbar.visibility = View.GONE
                }

        rxState.ofType(State.SetOptionsItemSelected::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val item = navigationView.menu.findItem(it.itemId)
                    item.isChecked = true
                    toolbar.title = item.title
                }

        dispatcher.dispatch(Screen.PinEntry)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.drawer_view, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        TransitionManager.beginDelayedTransition(container as ViewGroup, SlideOutOnly(Gravity.RIGHT))
        dispatcher.goBack()
    }
}