package hybrid.peddle.ui.checkout

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import hybrid.peddle.R
import hybrid.peddle.di.Injector
import hybrid.peddle.repository.models.ProductItem
import hybrid.peddle.ui.Dispatcher
import hybrid.peddle.ui.RxState
import hybrid.peddle.ui.Screen
import hybrid.peddle.ui.State
import hybrid.peddle.ui.base.BasePresenter
import hybrid.peddle.ui.base.MvpView
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.view_item_picker.view.*
import kotlinx.android.synthetic.main.view_item_picker_card.view.*
import java.math.BigDecimal
import javax.inject.Inject

class ItemPickerView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
        ConstraintLayout(context, attrs, defStyle), ItemPickerMvpView {
    @Inject
    lateinit var presenter: ItemPickerPresenter

    private val groupAdapter = GroupAdapter<ViewHolder>()

    init {
        Injector.obtain(getContext()).inject(this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        presenter.attachView(this)
        setupRecycler()
        // Setup listeners
    }

    override fun show() {
        visibility = View.VISIBLE
        var items = listOf(
                ProductItem("1", "Apple is the best thing for you", "Fruit", 25.00.toBigDecimal()),
                ProductItem("2", "Coke", "Fruit", 1.00.toBigDecimal()),
                ProductItem("3", "Fries", "Fruit", 1.50.toBigDecimal())
        )
        populateAdapter(items)
    }

    override fun hide() {
        visibility = View.INVISIBLE
    }

    override fun updateCart(items: List<ProductItem>){
        var totalCount = 0
        var totalCost : BigDecimal = 0.toBigDecimal()
        items.forEach  {
            totalCount += 1
            totalCost += it.price
        }
        val cost= "P %.2f".format(totalCost)
        uxCheckOutItems.text = "${totalCount} items ( ${cost} )"
    }

    private fun setupRecycler() {
        itemPickerRecycler.apply {
            adapter = groupAdapter
        }
    }

    private fun populateAdapter(items: List<ProductItem>) {
        groupAdapter.clear()
        items.forEach { groupAdapter.add(ResultItem(it)) }
        groupAdapter.setOnItemClickListener { item, view ->
            val productItem = (item as ResultItem).item

            presenter.addToCart(productItem)
        }
    }
}

class ItemPickerPresenter
@Inject constructor(val dispatcher: Dispatcher,
                    val rxState: RxState) : BasePresenter<ItemPickerMvpView>() {
    private val optionItemId: Int = R.id.navCheckout
    private var cart = mutableListOf<ProductItem>()

    @SuppressLint("CheckResult")
    override fun attachView(mvpView: ItemPickerMvpView) {
        super.attachView(mvpView)

        rxState.showing(Screen.ItemPicker.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mvpView.show()
                    dispatcher.dispatch(State.SetOptionsItemSelected(optionItemId))
                }

        rxState.showingNot(Screen.ItemPicker.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.hide() }

        rxState.ofType(State.SignOut::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.hide() }

        rxState.ofType(State.AddToCart::class.java)
                .subscribe {
                    cart.add(it.item)
                    mvpView.updateCart(cart)
                }
    }

    fun addToCart(item: ProductItem) {
        dispatcher.dispatch(State.AddToCart(item))
    }
}

interface ItemPickerMvpView : MvpView {
    fun show()
    fun hide()
    fun updateCart(items: List<ProductItem>)
}

class ResultItem(val item: ProductItem) : com.xwray.groupie.kotlinandroidextensions.Item() {
    override fun bind(viewHolder: com.xwray.groupie.kotlinandroidextensions.ViewHolder, position: Int) {
        viewHolder.itemView.uxItemCardTextAvatar.text = item.name
        viewHolder.itemView.uxItemName.text = item.name
        viewHolder.itemView.uxItemCategory.text = item.category
        viewHolder.itemView.uxItemPrice.text = "P %.2f".format(item.price)
    }

    override fun getLayout() = R.layout.view_item_picker_card
}
