package hybrid.peddle.ui.base

import android.animation.Animator
import android.view.ViewGroup
import androidx.transition.Slide
import androidx.transition.TransitionValues

class SlideOutOnly(slideEdge: Int) : Slide(slideEdge) {
    override fun onAppear(sceneRoot: ViewGroup?, startValues: TransitionValues?, startVisibility: Int, endValues: TransitionValues?, endVisibility: Int): Animator? {
        return null
    }
}
