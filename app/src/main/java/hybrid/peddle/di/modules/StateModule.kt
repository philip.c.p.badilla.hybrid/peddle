package hybrid.peddle.di.modules

import android.app.Activity
import dagger.Module
import dagger.Provides
import hybrid.peddle.di.qualifiers.ActivityScoped
import hybrid.peddle.ui.*
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

@Module
class StateModule {

    @Provides
    @ActivityScoped
    fun provideScreenContainer(activity: Activity): ScreenContainer {
        return object : ScreenContainer {
            override fun inflateAndAdd(layout: Int) {
                activity.layoutInflater.inflate(layout, activity.container, true)
            }
        }
    }

    @Provides
    @ActivityScoped
    fun provideMessageSubject(): PublishSubject<State> {
        return PublishSubject.create<State>()
    }

    @Provides
    @ActivityScoped
    fun provideBackStack(): Stack<Showing> {
        return Stack()
    }

    @Provides
    @ActivityScoped
    fun dispatcher(backStack: Stack<Showing>, rxState: RxState): Dispatcher {
        return RealDispatcher(backStack = backStack,rxState = rxState)
    }

    @Provides
    @ActivityScoped
    fun rxState(stream: PublishSubject<State>, backStack: Stack<Showing>): RxState {
        return RealRxState(stream, backStack)
    }
}