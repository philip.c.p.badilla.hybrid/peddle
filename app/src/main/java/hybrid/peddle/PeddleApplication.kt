package hybrid.peddle

import android.app.Application
import hybrid.peddle.di.Injector

class PeddleApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.createComponent(this)
    }
}
