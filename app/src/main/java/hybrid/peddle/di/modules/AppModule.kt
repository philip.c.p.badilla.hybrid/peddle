package hybrid.peddle.di.modules

import dagger.Module
import dagger.Provides
import java.util.*

@Module
class AppModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideUUID(): UUID {
            return UUID.randomUUID()
        }
    }
}