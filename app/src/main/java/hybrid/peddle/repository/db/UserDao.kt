package hybrid.peddle.repository.db

import androidx.room.*
import hybrid.peddle.repository.models.User

@Dao
interface UserDao : BaseDao<User> {

    @Transaction
    fun updateData(users: List<User>) {
        deleteAll()
        insertAll(users)
    }

    @Query("SELECT * FROM users")
    fun getAll(): List<User>

    @Query("DELETE FROM users")
    fun deleteAll()
}