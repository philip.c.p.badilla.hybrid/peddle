package hybrid.peddle.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import hybrid.peddle.di.modules.ActivityModule
import hybrid.peddle.di.modules.AppModule
import hybrid.peddle.di.modules.RoomModule
import hybrid.peddle.repository.PeddleDatabase
import hybrid.peddle.repository.UserRepository
import hybrid.peddle.repository.db.UserDao
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    RoomModule::class
])
interface AppComponent {
    @Component.Builder
    interface Builder {
        fun build(): AppComponent
        @BindsInstance
        fun application(application: Application): AppComponent.Builder
        fun room(roomModule: RoomModule): AppComponent.Builder
    }

    fun plusActivityComponent(activityModule: ActivityModule): ActivityComponent
}
