package hybrid.peddle.di

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import hybrid.peddle.di.modules.ActivityModule
import hybrid.peddle.di.modules.RoomModule

private const val ACTIVITY_COMPONENT = "ACTIVITY_COMPONENT"

object Injector {
    lateinit var appComponent: AppComponent

    fun createComponent(application: Application) {
        appComponent = DaggerAppComponent
                .builder()
                .application(application)
                .room(RoomModule(application))
                .build()
    }

    fun create(activity: Activity): ActivityComponent {
        return appComponent
                .plusActivityComponent(ActivityModule(activity))
    }

    fun matchesService(name: String): Boolean {
        return ACTIVITY_COMPONENT == name
    }

    @SuppressLint("WrongConstant")
    fun obtain(context: Context): ActivityComponent {
        return context.getSystemService(ACTIVITY_COMPONENT) as ActivityComponent
    }
}