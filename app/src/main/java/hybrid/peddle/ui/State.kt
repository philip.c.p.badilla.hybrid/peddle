package hybrid.peddle.ui

import hybrid.peddle.repository.models.ProductItem
import hybrid.peddle.repository.models.User

sealed class State {
    object GoingBack : State()
    data class UpdatingNumPadValueState(val value: String) : State()
    data class SettingPinValue(val value: String) : State()
    data class AddToCart(val item: ProductItem) : State()
    data class SessionStart(val user: User) : State()
    data class SetOptionsItemSelected(val itemId: Int) : State()
    object SignOut : State()
    object IncorrectPin : State()
}

data class Creating(val screen: Screen) : State()

open class Showing(val screen: Screen) : State() {
    object BackStackEmpty : Showing(Screen.Empty)
}

sealed class Screen : State() {

    var forward = true
    var replace = false

    object Empty : Screen()
    object PinEntry : Screen()
    object ProductCatalog : Screen()
    object UserCatalog : Screen()
    object ItemPicker : Screen()
}
