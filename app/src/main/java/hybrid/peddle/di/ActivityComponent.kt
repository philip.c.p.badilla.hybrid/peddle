package hybrid.peddle.di

import dagger.Subcomponent
import hybrid.peddle.di.modules.ActivityModule
import hybrid.peddle.di.modules.ScreenModule
import hybrid.peddle.di.modules.StateModule
import hybrid.peddle.di.qualifiers.ActivityScoped
import hybrid.peddle.ui.base.MainActivity
import hybrid.peddle.ui.checkout.ItemPickerView
import hybrid.peddle.ui.numpad.NumPadView
import hybrid.peddle.ui.numpad.PinEntryView
import hybrid.peddle.ui.products.ProductCatalogView
import hybrid.peddle.ui.users.UserCatalogView

@Subcomponent(modules = [
    ActivityModule::class,
    StateModule::class,
    ScreenModule::class
])
@ActivityScoped
interface ActivityComponent {
    fun inject(activity: MainActivity)
    fun inject(numPad: NumPadView)
    fun inject(pinEntry: PinEntryView)
    fun inject(userCatalog: ProductCatalogView)
    fun inject(userCatalog: UserCatalogView)
    fun inject(itemPicker: ItemPickerView)
}