package hybrid.peddle.repository.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(@PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "email") var email: String,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "password") var password: String,
        @ColumnInfo(name = "pin") var pin: String,
        @ColumnInfo(name = "active") var active: Boolean)