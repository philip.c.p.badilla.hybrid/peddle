package hybrid.peddle.di.modules

import dagger.Module
import dagger.Provides
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import hybrid.peddle.R
import hybrid.peddle.ui.Screen

@Module
class ScreenModule {
    @Provides
    @IntoMap
    @ClassKey(Screen.PinEntry::class)
    fun providePinEntryLayout():Int = R.layout.view_pin_entry

    @Provides
    @IntoMap
    @ClassKey(Screen.ProductCatalog::class)
    fun provideProductCatalogLayout():Int = R.layout.view_product_catalog

    @Provides
    @IntoMap
    @ClassKey(Screen.UserCatalog::class)
    fun provideUserCatalogLayout():Int = R.layout.view_user_catalog

    @Provides
    @IntoMap
    @ClassKey(Screen.ItemPicker::class)
    fun provideItemPickerLayout():Int = R.layout.view_item_picker
}