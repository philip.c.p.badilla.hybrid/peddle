package hybrid.peddle.ui.products

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import hybrid.peddle.R
import hybrid.peddle.di.Injector
import hybrid.peddle.ui.Dispatcher
import hybrid.peddle.ui.RxState
import hybrid.peddle.ui.Screen
import hybrid.peddle.ui.State
import hybrid.peddle.ui.base.BasePresenter
import hybrid.peddle.ui.base.MvpView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class ProductCatalogView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
        ConstraintLayout(context, attrs, defStyle), ProductCatalogMvpView {
    @Inject
    lateinit var presenter: ProductCatalogPresenter

    private val groupAdapter = GroupAdapter<ViewHolder>()

    init {
        Injector.obtain(getContext()).inject(this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        presenter.attachView(this)

        // Setup listeners
    }

    override fun show() {
        visibility = View.VISIBLE
    }

    override fun hide() {
        visibility = View.INVISIBLE
    }
}

class ProductCatalogPresenter
@Inject constructor(val dispatcher: Dispatcher,
                    val rxState: RxState) : BasePresenter<ProductCatalogMvpView>() {
    private val optionItemId: Int = R.id.navProducts

    @SuppressLint("CheckResult")
    override fun attachView(mvpView: ProductCatalogMvpView) {
        super.attachView(mvpView)

        rxState.showing(Screen.ProductCatalog.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mvpView.show()
                    dispatcher.dispatch(State.SetOptionsItemSelected(optionItemId))
                }

        rxState.showingNot(Screen.ProductCatalog.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.hide() }

        rxState.ofType(State.SignOut::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.hide() }
    }
}

interface ProductCatalogMvpView : MvpView {
    fun show()
    fun hide()
}
