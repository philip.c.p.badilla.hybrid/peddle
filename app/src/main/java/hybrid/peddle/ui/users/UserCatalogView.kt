package hybrid.peddle.ui.users

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import hybrid.peddle.R
import hybrid.peddle.di.Injector
import hybrid.peddle.ui.Dispatcher
import hybrid.peddle.ui.RxState
import hybrid.peddle.ui.Screen
import hybrid.peddle.ui.State
import hybrid.peddle.ui.base.BasePresenter
import hybrid.peddle.ui.base.MvpView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class UserCatalogView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
        ConstraintLayout(context, attrs, defStyle), UserCatalogMvpView {
    @Inject
    lateinit var presenter: UserCatalogPresenter

    init {
        Injector.obtain(getContext()).inject(this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        presenter.attachView(this)

        // Setup listeners
    }

    override fun show() {
        visibility = View.VISIBLE
    }

    override fun hide() {
        visibility = View.INVISIBLE
    }
}

class UserCatalogPresenter
@Inject constructor(val dispatcher: Dispatcher,
                    val rxState: RxState) : BasePresenter<UserCatalogMvpView>() {
    private val optionItemId: Int = R.id.navUsers

    @SuppressLint("CheckResult")
    override fun attachView(mvpView: UserCatalogMvpView) {
        super.attachView(mvpView)

        rxState.showing(Screen.UserCatalog.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mvpView.show()
                    dispatcher.dispatch(State.SetOptionsItemSelected(optionItemId))
                }

        rxState.showingNot(Screen.UserCatalog.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.hide() }

        rxState.ofType(State.SignOut::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.hide() }
    }
}

interface UserCatalogMvpView : MvpView {
    fun show()
    fun hide()
}
