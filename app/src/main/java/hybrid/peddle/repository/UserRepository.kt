package hybrid.peddle.repository

import hybrid.peddle.repository.db.UserDao
import hybrid.peddle.repository.models.User
import io.reactivex.Maybe
import io.reactivex.Observable
import javax.inject.Inject

class UserDataSource
@Inject constructor(private val userDao: UserDao) : UserRepository {
    override fun searchByPin(pin: Int): Maybe<User> {
        return Maybe
                .fromCallable {
                    userDao.getAll().firstOrNull { it.pin == pin.toString() }
                }
    }

    override fun createUser(user: User): Observable<User> {
        return Observable
                .fromCallable {
                    userDao.insert(user)
                    user
                }

    }

    override fun getAll(): Observable<List<User>> {
        return Observable
                .fromCallable {
                    userDao.getAll()
                }
    }
}

interface UserRepository {
    fun searchByPin(pin: Int): Maybe<User>

    fun getAll(): Observable<List<User>>

    fun createUser(user: User): Observable<User>
}