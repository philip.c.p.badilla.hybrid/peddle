package hybrid.peddle.ui.numpad

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import hybrid.peddle.R
import hybrid.peddle.di.Injector
import hybrid.peddle.ui.*
import hybrid.peddle.ui.base.BasePresenter
import hybrid.peddle.ui.base.MvpView
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.view_num_pad.view.*
import javax.inject.Inject

class NumPadView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
        ConstraintLayout(context, attrs, defStyle), NumPadMvpView {
    @Inject
    lateinit var presenter: NumPadPresenter

    var valueState: String = ""

    init {
        Injector.obtain(getContext()).inject(this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        presenter.attachView(this)

        setUpListeners()
    }

    private fun setUpListeners() {
        numPad0.setOnClickListener {
            onPadClick(it)
        }
        numPad1.setOnClickListener {
            onPadClick(it)
        }
        numPad2.setOnClickListener {
            onPadClick(it)
        }
        numPad3.setOnClickListener {
            onPadClick(it)
        }
        numPad4.setOnClickListener {
            onPadClick(it)
        }
        numPad5.setOnClickListener {
            onPadClick(it)
        }
        numPad6.setOnClickListener {
            onPadClick(it)
        }
        numPad7.setOnClickListener {
            onPadClick(it)
        }
        numPad8.setOnClickListener {
            onPadClick(it)
        }
        numPad9.setOnClickListener {
            onPadClick(it)
        }
        numPadBack.setOnClickListener {
            onPadClick(it)
        }
        numPadClear.setOnClickListener {
            onPadClick(it)
        }
    }

    private fun onPadClick(it: View) {
        valueState = when (it.id) {
            R.id.numPadClear -> ""
            R.id.numPadBack -> valueState.dropLast(1)
            else -> valueState + it.tag
        }

        presenter.updateValueState(valueState)
    }

    override fun show() {
        visibility = View.VISIBLE
    }

    override fun hide() {
        visibility = View.INVISIBLE
    }

    override fun updateValue(value: String) {
        valueState = value
        presenter.updateValueState(valueState)
    }
}

class NumPadPresenter
@Inject constructor(val dispatcher: Dispatcher,
                    val rxState: RxState) : BasePresenter<NumPadMvpView>() {

    @SuppressLint("CheckResult")
    override fun attachView(mvpView: NumPadMvpView) {
        super.attachView(mvpView)

        rxState.showing(Screen.PinEntry.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.show() }

        rxState.showingNot(Screen.PinEntry.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.hide() }

        rxState.ofType(State.SettingPinValue::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.updateValue(it.value) }
    }

    fun updateValueState(value: String) {
        dispatcher.dispatch(State.UpdatingNumPadValueState(value))
    }
}

interface NumPadMvpView : MvpView {
    fun show()
    fun hide()
    fun updateValue(value: String)
}