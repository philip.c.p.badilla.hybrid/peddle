package hybrid.peddle.ui

import hybrid.peddle.di.qualifiers.ActivityScoped
import timber.log.Timber
import java.util.*

interface Dispatcher {
    fun dispatch(state: State)
    fun goBack()
    fun popScreensAndGoBack(vararg screenClasses: Class<out Screen>)
    fun clearBackStack()
}

@ActivityScoped
class RealDispatcher
constructor(val rxState: RxState, private val backStack: Stack<Showing>) : Dispatcher {
    override fun dispatch(state: State) {
        when (state) {
            is Screen -> {
                state.forward = true
                rxState.push(Creating(state))
            }
            is Showing -> {
                if (state.screen.replace && !backStack.empty()) backStack.pop()
                if (backStack.empty() || backStack.peek().screen != state.screen) backStack.push(state)
                rxState.push(state)
                Timber.d("pushing %s", backStack.peek().screen.toString())
            }
            else -> rxState.push(state)
        }
    }

    override fun goBack() {
        dispatch(State.GoingBack)
        popLastShowingState()//current state is what we are currently showing
        reShowTopScreen()
    }

    override fun popScreensAndGoBack(vararg screenClasses: Class<out Screen>) {
        while (backStack.isNotEmpty()) {
            val topScreen = backStack.peek()
            if (topScreen.screen::class.java in screenClasses) {
                backStack.pop()
            } else {
                break
            }
        }
        reShowTopScreen()
    }

    override fun clearBackStack() {
        backStack.clear()
    }

    private fun reShowTopScreen() {
        val backTo = popLastShowingState()
        backTo.screen.forward = false
        dispatch(backTo)
    }

    private fun popLastShowingState(): Showing {

        return if (backStack.isEmpty()) {
            Showing.BackStackEmpty
        } else {
            Timber.d("popping " + backStack.peek())
            backStack.pop()
        }
    }
}
