package hybrid.peddle.di.modules

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import hybrid.peddle.repository.PeddleDatabase
import hybrid.peddle.repository.UserDataSource
import hybrid.peddle.repository.UserRepository
import hybrid.peddle.repository.db.UserDao
import javax.inject.Singleton

@Module
class RoomModule(val application: Application) {

    @Provides
    @Singleton
    fun provideRoomDatabase(): PeddleDatabase {
        //return Room.databaseBuilder(application.applicationContext, PeddleDatabase::class.java, "peddle_db").build()
        return PeddleDatabase.getInstance(application.applicationContext)
    }

    @Provides
    @Singleton
    fun provideUserDao(peddleDatabase: PeddleDatabase): UserDao {
        return peddleDatabase.userDao()
    }

    @Provides
    @Singleton
    fun provideRepository(userDao: UserDao): UserRepository {
        return UserDataSource(userDao)
    }
}