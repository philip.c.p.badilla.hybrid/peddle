package hybrid.peddle.ui.numpad

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import hybrid.peddle.di.Injector
import hybrid.peddle.repository.UserRepository
import hybrid.peddle.repository.models.User
import hybrid.peddle.ui.Dispatcher
import hybrid.peddle.ui.RxState
import hybrid.peddle.ui.Screen
import hybrid.peddle.ui.State
import hybrid.peddle.ui.base.BasePresenter
import hybrid.peddle.ui.base.MvpView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.view_pin_entry.view.*
import timber.log.Timber
import javax.inject.Inject

class PinEntryView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
        ConstraintLayout(context, attrs, defStyle), PinEntryMvpView {

    @Inject
    lateinit var presenter: PinEntryPresenter

    init {
        Injector.obtain(getContext()).inject(this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        presenter.attachView(this)

        // Setup listeners
    }

    override fun show() {
        visibility = View.VISIBLE
    }

    override fun hide() {
        visibility = View.INVISIBLE
    }

    override fun updatePinValue(value: String) {
        uxPinDisplay.text = value.replace(regex = Regex("""[$0-9]"""), replacement = "*")
        validatePin(value)
    }

    override fun showIncorrectPinNotice() {
        val toast = Toast.makeText(context, "Invalid PIN", Toast.LENGTH_SHORT)
        toast.show()
    }

    private fun validatePin(value: String) {
        if (value.length < 6) return

        presenter.login(value)
    }
}

class PinEntryPresenter
@Inject constructor(val dispatcher: Dispatcher,
                    val rxState: RxState,
                    private val userRepository: UserRepository
        ) : BasePresenter<PinEntryMvpView>() {

    @SuppressLint("CheckResult")
    override fun attachView(mvpView: PinEntryMvpView) {
        super.attachView(mvpView)

        rxState.showing(Screen.PinEntry.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.show() }

        rxState.showingNot(Screen.PinEntry.javaClass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.hide() }

        rxState.ofType(State.SignOut::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { resetPinEntry() }

        rxState.ofType(State.UpdatingNumPadValueState::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.updatePinValue(it.value) }

        rxState.ofType(State.IncorrectPin::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { mvpView.showIncorrectPinNotice() }
    }

    @SuppressLint("CheckResult")
    fun login(value: String) {
        userRepository.searchByPin(value.toInt())
                // .doOnSubscribe { dispatcher.dispatch(State.Loading) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            startSession(it)
                        },
                        {
                            Timber.d("An error occurred while logging in %s", it.message)
                        },
                        {
                            dispatcher.dispatch(State.IncorrectPin)
                            resetPinEntry()
                        }
                )
    }

    private fun startSession(user: User) {
        // Do stuff to initialize user session
        dispatcher.clearBackStack()
        dispatcher.dispatch(Screen.ItemPicker)
        dispatcher.dispatch(State.SessionStart(user))
    }

    private fun resetPinEntry() {
        dispatcher.dispatch(State.SettingPinValue(""))
    }
}

interface PinEntryMvpView : MvpView {
    fun show()
    fun hide()
    fun showIncorrectPinNotice()
    fun updatePinValue(value: String)
}